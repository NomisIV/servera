# Servera web server

This is the web server for my personal website,
but I've written it to be as general as possible.

## How do I use it?

```
servera PORT [ROOT]

PORT should be an available port

ROOT is the root directory of the website. Defaults to the current working directory
```

## Why should I use it?

This web server is incredibly minimal in design.
It's purpose is simply to serve a directory to a port, just how I want it to.
So if you want to serve your files just like I want to serve my files,
this is very much for you

## Cool featues

### Async and multithreaded (sponsored by tokio and hyper)

The web server is based on the
[tokio](https://tokio.rs/)
and
[hyper](https://hyper.rs/)
crates which are very performant.

### Custom error pages

The web server automatically tries to load custom error pages from the `/errors` directory.
For example:
```
Error 404 -> /errors/404.html
```

### Very dynamic URI resolving

The web server uses an **advanced** algorithm to find the potential files when resolving URIs.
For example:
```
http://example.com/page.html -> page.html

http://example.com/page      -> ./page
                             -> ./page.html
                             -> http://example.com/page/ (if /page/ is a directory)

http://example.com/page/     -> ./page/index.html
                             -> http://example.com/page (if /page is a file)
```
