{
  description = "The flake for the servera web server";

  outputs = {
    self,
    nixpkgs,
  }: let
    systems = ["x86_64-linux" "aarch64-linux"];

    config = system: let
      pkgs = nixpkgs.legacyPackages.${system};
      cargoToml = builtins.fromTOML (builtins.readFile ./Cargo.toml);
    in {
      packages.${system}.default = pkgs.rustPlatform.buildRustPackage {
        pname = cargoToml.package.name;
        version = cargoToml.package.version;
        src = ./.;
        cargoLock.lockFile = ./Cargo.lock;
      };

      nixosModule = import ./module.nix;

      devShells.${system}.default = pkgs.mkShell {
        buildInputs = with pkgs; [rustc cargo rustfmt rust-analyzer nil];
      };

      formatter.${system} = pkgs.alejandra;
    };

    mergeSystems = acc: system:
      with nixpkgs.legacyPackages.${system};
        lib.attrsets.recursiveUpdate acc (config system);
  in
    builtins.foldl' mergeSystems {} systems;
}
