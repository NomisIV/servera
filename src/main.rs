use hyper::header;
use hyper::server::conn::AddrStream;
use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Method, Request, Response, Server, StatusCode};
use std::convert::Infallible;
use std::env;
use std::error::Error;
use std::fmt::{self, Debug, Display, Formatter};
use std::net::SocketAddr;
use std::path::PathBuf;
use std::process;
use tokio::fs;
use tokio_util::io::ReaderStream;

const USAGE: &str = r#"
A minimal static http server

USAGE:
    servera PORT [ROOT]

PORT:
    Should be an available port

ROOT:
    The root directory of the website to be served.
    Defaults to the current working directory.

EXAMPLES:
    servera 8000
    servera 1337 ../website
    servera 6969 /var/www/html

servera was written by Nomisiv <simon@nomisiv.com>
Report bugs to https://github.com/NomisIV/servera/issues
"#;

#[derive(Clone)]
struct AppContext {
    root: PathBuf,
}

type AsyncError = Box<dyn Error + Send + Sync>;

struct MimeGuessError;

impl Display for MimeGuessError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_str("Could not guess mime data")
    }
}

impl Debug for MimeGuessError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.write_str("Could not guess mime data")
    }
}

impl Error for MimeGuessError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

async fn shutdown_signal() {
    // Wait for the CTRL+C signal
    tokio::signal::ctrl_c()
        .await
        .expect("failed to install CTRL+C signal handler");
}

async fn get(path: PathBuf) -> Result<Response<Body>, AsyncError> {
    let file = fs::File::open(&path).await?;
    let stream = ReaderStream::new(file);
    let mime = mime_guess::from_path(&path)
        .first_raw()
        .ok_or(MimeGuessError)?;

    Ok(Response::builder()
        .status(StatusCode::OK)
        .header(header::CONTENT_TYPE, mime)
        .body(Body::wrap_stream(stream))?)
}

fn redirect(uri: &str) -> Result<Response<Body>, AsyncError> {
    Ok(Response::builder()
        .status(StatusCode::MOVED_PERMANENTLY)
        .header(header::LOCATION, uri)
        .body(Body::empty())?)
}

async fn error(status: StatusCode, context: &AppContext) -> Response<Body> {
    let err_path = context
        .root
        .join(PathBuf::from(format!("errors/{}.html", status.as_str())));

    let body = if err_path.is_file() {
        let file = fs::File::open(err_path).await.unwrap();
        let stream = ReaderStream::new(file);
        Body::wrap_stream(stream)
    } else {
        match status.as_u16() {
            404 => "404 Not Found :(",
            500 => "500 Internal Server Error :(",
            _ => ":(",
        }
        .into()
    };

    Response::builder().status(status).body(body).unwrap()
}

async fn serve(
    req: Request<Body>,
    ip: SocketAddr,
    context: AppContext,
) -> Result<Response<Body>, hyper::Error> {
    let response = match req.method() {
        &Method::GET => {
            let uri = req.uri().path().to_string();
            let path = context.root.join(uri.strip_prefix("/").unwrap());

            let result = if uri.ends_with("/") {
                // Assume the uri points to a folder
                if path.is_dir() {
                    let path = path.join("index.html");
                    if path.is_file() {
                        get(path).await
                    } else {
                        Ok(error(StatusCode::NOT_FOUND, &context).await)
                    }
                } else if path.is_file() {
                    redirect(uri.strip_suffix("/").unwrap())
                } else {
                    Ok(error(StatusCode::NOT_FOUND, &context).await)
                }
            } else {
                // Assume the uri points to a file
                if path.is_file() {
                    get(path).await
                } else if path.extension() == None && path.with_extension("html").is_file() {
                    get(path.with_extension("html")).await
                } else if path.is_dir() {
                    redirect(&(uri + "/"))
                } else {
                    Ok(error(StatusCode::NOT_FOUND, &context).await)
                }
            };

            match result {
                Ok(response) => response,
                Err(e) => {
                    eprintln!("{}", e);
                    error(StatusCode::INTERNAL_SERVER_ERROR, &context).await
                }
            }
        }

        _ => Response::builder()
            .status(StatusCode::METHOD_NOT_ALLOWED)
            .body(Body::empty())
            .unwrap(),
    };

    let log_line = format!(
        "[{status}] {ip} {method} {uri}",
        status = response.status().as_str(),
        ip = ip,
        method = req.method(),
        uri = req.uri().path()
    );

    let status = response.status();

    if status.is_informational() || status.is_success() || status.is_redirection() {
        println!("{}", log_line);
    } else {
        eprintln!("{}", log_line);
    }

    Ok(response)
}

#[tokio::main]
async fn main() {
    let args = env::args().collect::<Vec<String>>();

    if args.len() < 2 || args.len() > 3 {
        println!("{}", USAGE);
        process::exit(64);
    }

    let port = args.get(1).unwrap().parse().unwrap();
    let addr = SocketAddr::from(([0, 0, 0, 0], port));

    let dir = args
        .get(2)
        .map(|dir_str| PathBuf::from(dir_str))
        .unwrap_or(env::current_dir().unwrap());

    let context = AppContext { root: dir };

    let make_service = make_service_fn(move |conn: &AddrStream| {
        let context = context.clone();
        let ip = conn.remote_addr();
        let service = service_fn(move |req| serve(req, ip, context.clone()));
        async move { Ok::<_, Infallible>(service) }
    });

    let server = Server::bind(&addr).serve(make_service);

    let graceful = server.with_graceful_shutdown(shutdown_signal());

    println!("Starting server on http://localhost:{}", port);

    // Run this server for... forever!
    if let Err(e) = graceful.await {
        eprintln!("server error: {}", e);
    }
}
