{
  lib,
  config,
  pkgs,
  ...
}: let
  cfg = config.services.servera;
in {
  options.services.servera = with lib; {
    enable = mkEnableOption "servera web server";

    openFirewall = mkEnableOption "open the port for servera in the firewall";

    port = mkOption {
      type = types.port;
      default = 8000;
      description = "The port to serve at";
    };

    dataDir = mkOption {
      type = types.str;
      default = "/var/lib/servera";
      description = "The location of the web root";
    };

    user = mkOption {
      type = types.str;
      default = "servera";
      description = "The user to run servera as";
    };

    group = mkOption {
      type = types.str;
      default = "servera";
      description = "The group to run servera as";
    };
  };

  config = lib.mkIf cfg.enable {
    # Main servera service
    systemd.services.servera = {
      description = "servera web server";
      after = ["network.target"];
      requires = ["network.target"];
      wantedBy = ["multi-user.target"];

      serviceConfig = {
        ExecStart = "${pkgs.servera}/bin/servera ${toString cfg.port} ${cfg.dataDir}";
        User = cfg.user;
        Group = cfg.group;
      };
    };

    # Open firewall
    networking.firewall = lib.mkIf cfg.openFirewall {
      allowedTCPPorts = [cfg.port];
    };

    # Add user and group
    users.users = lib.mkIf (cfg.user == "servera") {
      servera = {
        group = cfg.group;
        home = cfg.dataDir;
        uid = 350;
      };
    };

    users.groups = lib.mkIf (cfg.group == "servera") {
      servera.gid = 350;
    };
  };
}
